function [serialNumber] = extractTestDataFromVideos(input_path,output_path,...
    file,serialNumber,video_num,rotation,resize,rescaling_var)

if ispc
    PATHSEP = '\';
else
    PATHSEP = '/';
end

% if nargin > 7
%    rescaling_var = vararging{1};
% else
%     rescaling_var = width;
% end

prdID = input_path(end-5:1:end);

video_path = [input_path PATHSEP file];

videoIsRead = 0;

while ~videoIsRead
    try
    videoObj = VideoReader(video_path);
    videoIsRead = 1;
        disp(['Video #',num2str(video_num),' successfully read']);
    catch
        videoIsRead = 0;
        disp('Unable to read video');
    end
end
lastFrame = read(videoObj, inf);
numFrames = videoObj.NumberOfFrames;

    for ii = 1:numFrames
        if(rotation)
            rotated_frame = imrotate(read(videoObj,ii),180);
            if resize == 1
                frame = imresize(rotated_frame,rescaling_var);
            elseif resize == 2
                frame = imresize(rotated_frame,[NaN rescaling_var]);
            else
                frame = rotated_frame;
            end
        else
            if resize == 1
                frame = imresize(read(videoObj,ii),rescaling_var);
            elseif resize == 2
                frame = imresize(read(videoObj,ii),[NaN rescaling_var]);
            else
                frame = read(videoObj,ii);
            end
        end
        img_filename = [sprintf('%05d',serialNumber) '.jpg'];

        imwrite(frame,[output_path PATHSEP img_filename],'Quality',100);
        
        % Create the annotation for the image.
        
%         annotation(serialNumber).img_name = img_filename;
% %         annotation(tot_img_number).Bbox = rect;
%         annotation(serialNumber).class = prdID;
        serialNumber = serialNumber + 1;
    end %end foor loop

  
end %end function