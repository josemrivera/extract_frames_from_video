function [num_videos,videos] = getVideosInFolder(path)

files = dir(path);    
files = files(3:end);
num_files = length(files);
num_videos = 0;

%% do a quick scan to find number of images in folder

for i=1:num_files
    
    f = files(i);
    [~,~,ext] = fileparts(f.name);    
    
    %check if it is a directory
    if( f.isdir==1 )
        continue;
    end

    %check if it is a video file
    if ( ~strcmpi(ext,'.mov') && ~strcmpi(ext,'.mp4') && ...
            ~strcmpi(ext,'.mpeg') && ~strcmpi(ext,'.mjpeg') ... 
            && ~strcmpi(ext,'.3gp') && ~strcmpi(ext,'.avi'))            
        continue;
    end
    
    num_videos=num_videos+1;
    
    videos(num_videos) = files(i);

end % end function