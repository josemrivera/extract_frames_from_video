function [] = extractFrames(varargin)

% EXTRACTFRAMES is a function that runs a frame extraction
% routine on all the videos present in the specified input location.
%
% Usage: 
%   a) Choosing parameters at prompt
%   extractFrames(/path/to/videos/);
%   
%   extractFrames(video_locations.txt) % In this case, 
%       video_locations.txt contains one path per line.
%
%   b) Passing parameters as inputs:
%   extractFrames(/path/to/videos/,rotation,resize,resize_factor);
%       - rotation takes 0 (no 90 rotation) or 1 (with 90 rotation)
%       - resize takes 0 (no resize), 1 (resize with resize factor),
%         2 resize adjusting the width of the extracted frames.
%       - resize factor: rescale factor (0-1) if resize == 1 or width in
%       pixels if resize == 2.
%
%   e.g. extractFrames(/path/to/videos/,1,1,0.5);
%           -> Rotates the frames 90 degrees, resizes the image to a half 
%              the resolution in every dimension.
%   e.g.2 extractFrames(/path/to/videos/,0,2,208);
%           -> No rotation and rescales the image so the horizontal
%           dimension has 208 pixels wide.
%
% Current supported formats: AVI,MOV,MP4,MJPEG,MPEG,3GP
%
% Author: Jose Rivera (jose.rivera@imperial.ac.uk)
%
% Date: v1.0 11/2011
%

try
    if(isdir(varargin{1}))
        video_path = varargin{1};
        parameters = varargin;
        parameters(1) = [];
        extractFrameVideosInFolder(video_path,parameters);
    else
        
        fid = fopen(varargin{1},'r');
        parameters = varargin;
        parameters(1) = [];
        strings = textscan(fid,'%s');
        video_paths = strings{1};
        
        for i = 1:length(video_paths)
            video_path = video_paths{i};
            extractFrameVideosInFolder(video_path,parameters);
        end
    end
catch
    msg = ['Not enough parameters. Usage:\n',...
    'extractFrameVideosInFolder(/path/to/videos/);\n',...
    'extractFrameVideosInFolder(video_locations.txt)'...
    '\nIn this case, video_locations.txt contains one path per line.\n'];
    
    error(sprintf(msg));
end


function  [] = extractFrameVideosInFolder(video_path,parameters)

% Multiplatform support

if ispc
    PATHSEP = '\';
else
    PATHSEP = '/';
end

    
[num_videos,videos] = getVideosInFolder(video_path);

serialNumber = 1;  % Variable to keep the frame count


if ~isempty(parameters) % Parameters specified at function call

    %% Hard coding

    rotation = parameters{1};
    resize = parameters{2};
    rescale_factor = parameters{3};
    
    if resize
        if resize == 1
            mkdir(video_path,['frames_resized_' num2str(rescale_factor)]);
            output_path = [video_path,PATHSEP,'frames_resized_',num2str(rescale_factor)];
        elseif resize == 2
            mkdir(video_path,['frames_resized_w' num2str(rescale_factor) 'p']);
            output_path = [video_path,PATHSEP,'frames_resized_w',num2str(rescale_factor) 'p'];
        end
    else
        mkdir(video_path,'frames');
        output_path = [video_path,PATHSEP,'frames'];
    end
    
    for ii = 1:num_videos

        file = videos(ii).name;
        [serialNumber] = extractTestDataFromVideos(video_path,output_path,file,serialNumber,ii,rotation,resize,rescale_factor);

    end %end for
    
else % User entered parameters

    disp('Does the video require rotation adjustments? ');
    rotation = input('(1=yes; 0=no):');

    disp('Would you like to resize?');
    resize = input('(1=yes, with a given scale; 2=yes, specifying the width, 0=no):');
    
    switch resize
        case 1
            resize_factor = input('Please specify your scale. ([] = 0.5): ');
            rescaling_var = resize_factor;
        case 2
            rescale_factor = input('Please specify the width in pixels. ([] = 320): ');
            rescaling_var = rescale_factor;
        otherwise
            disp('No resize will be performed');
    end

    if resize
        if resize == 1
            mkdir(video_path,['frames_resized_' num2str(resize_factor)]);
            output_path = [video_path,PATHSEP,'frames_resized_',num2str(resize_factor)];
        elseif resize == 2
            mkdir(video_path,['frames_resized_w' num2str(rescale_factor) 'p']);
            output_path = [video_path,PATHSEP,'frames_resized_w',num2str(rescale_factor) 'p'];
        end
    else
        mkdir(video_path,'frames');
        output_path = [video_path,PATHSEP,'frames'];
    end
    
    for ii = 1:num_videos

        file = videos(ii).name;
        [serialNumber] = extractTestDataFromVideos(video_path,output_path,file,serialNumber,ii,rotation,resize,rescaling_var);

    end %end for
    
end


disp ('Finished with the frame extraction');
    
clear all;

end % end extractFrameVideosInFolder
end % end function extractFrames